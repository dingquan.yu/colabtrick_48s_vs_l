"""Full AlphaFold protein structure prediction script."""
import json
import os
import pathlib
import pickle
import random
import shutil
import sys
import time
from typing import Dict, Union, Optional


from absl import app
from absl import flags
from absl import logging
from alphafold.common import protein
from alphafold.common import residue_constants
# from alphafold.data import pipeline
# from alphafold.data import pipeline_multimer
import templates
from alphafold.data.tools import hhsearch
from alphafold.data.tools import hmmsearch
from alphafold.model import config
from alphafold.model import model
from alphafold.relax import relax
import numpy as np

import os
from typing import Any, Mapping, MutableMapping, Optional, Sequence, Union
from absl import logging
from alphafold.common import residue_constants
from alphafold.data import msa_identifiers
from alphafold.data import parsers
from alphafold.data.tools import hhblits
from alphafold.data.tools import hhsearch
from alphafold.data.tools import hmmsearch
from alphafold.data.tools import jackhmmer
import numpy as np

# Internal import (7716).


FeatureDict = MutableMapping[str, np.ndarray]


from template_only_pipeline import DataPipeline


from alphafold.model import data
# Internal import (7716).


logging.set_verbosity(logging.INFO)


flags.DEFINE_list(
    'fasta_paths', None, 'Paths to FASTA files, each containing a prediction '
    'target that will be folded one after another. If a FASTA file contains '
    'multiple sequences, then it will be folded as a multimer. Paths should be '
    'separated by commas. All FASTA paths must have a unique basename as the '
    'basename is used to name the output directories for each prediction.')
# flags.DEFINE_string('fasta_paths',None,'path to fasta file')
flags.DEFINE_list(
    'is_prokaryote_list', None, 'Optional for multimer system, not used by the '
    'single chain system. This list should contain a boolean for each fasta '
    'specifying true where the target complex is from a prokaryote, and false '
    'where it is not, or where the origin is unknown. These values determine '
    'the pairing method for the MSA.')

flags.DEFINE_string('fasta_name', None, 'name of uniprot.')
flags.DEFINE_string('data_dir', None, 'Path to directory of supporting data.')
flags.DEFINE_string('output_dir', None, 'Path to a directory that will '
                    'store the results.')
flags.DEFINE_string('jackhmmer_binary_path', shutil.which('jackhmmer'),
                    'Path to the JackHMMER executable.')
flags.DEFINE_string('hhblits_binary_path', shutil.which('hhblits'),
                    'Path to the HHblits executable.')
flags.DEFINE_string('mmcif_dir',None,'Path to pdb mmcif files')
flags.DEFINE_string('hhsearch_binary_path', shutil.which('hhsearch'),
                    'Path to the HHsearch executable.')
flags.DEFINE_string('hmmsearch_binary_path', shutil.which('hmmsearch'),
                    'Path to the hmmsearch executable.')
flags.DEFINE_string('hmmbuild_binary_path', shutil.which('hmmbuild'),
                    'Path to the hmmbuild executable.')
flags.DEFINE_string('kalign_binary_path', shutil.which('kalign'),
                    'Path to the Kalign executable.')
flags.DEFINE_string('uniref90_database_path', None, 'Path to the Uniref90 '
                    'database for use by JackHMMER.')
flags.DEFINE_string('mgnify_database_path', None, 'Path to the MGnify '
                    'database for use by JackHMMER.')
flags.DEFINE_string('bfd_database_path', None, 'Path to the BFD '
                    'database for use by HHblits.')
flags.DEFINE_string('small_bfd_database_path', None, 'Path to the small '
                    'version of BFD used with the "reduced_dbs" preset.')
flags.DEFINE_string('uniclust30_database_path', None, 'Path to the Uniclust30 '
                    'database for use by HHblits.')
flags.DEFINE_string('uniprot_database_path', None, 'Path to the Uniprot '
                    'database for use by JackHMMer.')
flags.DEFINE_string('pdb70_database_path', None, 'Path to the PDB70 '
                    'database for use by HHsearch.')
flags.DEFINE_string('pdb_seqres_database_path', None, 'Path to the PDB '
                    'seqres database for use by hmmsearch.')

flags.DEFINE_string('max_template_date',None,'Max template date')
flags.DEFINE_enum('db_preset', 'full_dbs',
                  ['full_dbs', 'reduced_dbs'],
                  'Choose preset MSA database configuration - '
                  'smaller genetic database config (reduced_dbs) or '
                  'full genetic database config  (full_dbs)')
flags.DEFINE_enum('model_preset', 'monomer',
                  ['monomer', 'monomer_casp14', 'monomer_ptm', 'multimer'],
                  'Choose preset model configuration - the monomer model, '
                  'the monomer model with extra ensembling, monomer model with '
                  'pTM head, or multimer model')
flags.DEFINE_boolean('benchmark', False, 'Run multiple JAX model evaluations '
                     'to obtain a timing that excludes the compilation time, '
                     'which should be more indicative of the time required for '
                     'inferencing many proteins.')
flags.DEFINE_integer('random_seed', None, 'The random seed for the data '
                     'pipeline. By default, this is randomly generated. Note '
                     'that even if this is set, Alphafold may still not be '
                     'deterministic, because processes like GPU inference are '
                     'nondeterministic.')
flags.DEFINE_boolean('use_precomputed_msas', False, 'Whether to read MSAs that '
                     'have been written to disk. WARNING: This will not check '
                     'if the sequence, database or configuration have changed.')
flags.DEFINE_string('obsolete_pdbs_path',None,'obsolete data')

FLAGS = flags.FLAGS


MAX_TEMPLATE_HITS = 20
RELAX_MAX_ITERATIONS = 0
RELAX_ENERGY_TOLERANCE = 2.39
RELAX_STIFFNESS = 10.0
RELAX_EXCLUDE_RESIDUES = []
RELAX_MAX_OUTER_ITERATIONS = 3

FeatureDict = MutableMapping[str, np.ndarray]
# TemplateSearcher = Union[hhsearch.HHSearch, hmmsearch.Hmmsearch]
# TemplateSearcher = hhsearch.HHSearch(binary_path='/g/easybuild/x86_64/CentOS/7/haswell/software/HH-suite/3.3.0-gompic-2020b/bin/hhsearch',
# databases=['/scratch/AlphaFold_DBs/pdb70/pdb70'])

# Template_featuriser = templates.HhsearchHitFeaturizer(
#     mmcif_dir='/scratch/AlphaFold_DBs/pdb_mmcif/mmcif_files/',
#     max_template_date='2050-01-01',
#     max_hits=4,
#     kalign_binary_path='/g/easybuild/x86_64/CentOS/7/haswell/software/Kalign/3.3.1-GCCcore-10.2.0/bin/kalign',
#     obsolete_pdbs_path='/scratch/AlphaFold_DBs/pdb_mmcif/obsolete.dat',
#     release_dates_path=None
# )


def run_msa_tool(msa_runner, input_fasta_path: str, msa_out_path: str,
                 msa_format: str, use_precomputed_msas: bool,
                 ) -> Mapping[str, Any]:
  """Runs an MSA tool, checking if output already exists first."""
  if not use_precomputed_msas or not os.path.exists(msa_out_path):
    result = msa_runner.query(input_fasta_path[0])[0]
    with open(msa_out_path, 'w') as f:
      f.write(result[msa_format])
  else:
    logging.warning('Reading MSA from file %s', msa_out_path)
    with open(msa_out_path, 'r') as f:
      result = {msa_format: f.read()}
  return result



def make_template(
fasta_path: str,
fasta_name: str,
output_dir_base: str,
is_prokaryote: Optional[bool] = None):

    timings = {}
    output_dir = os.path.join(output_dir_base, fasta_name)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    else:
        print("outputdir exits:{}".format(output_dir))
    msa_output_dir = os.path.join(output_dir, 'msas')
    if not os.path.exists(msa_output_dir):
        os.makedirs(msa_output_dir)
    else:
        print("msa outputdir exits:{}".format(msa_output_dir))

## 
## starts from here 
##
    monomer_data_pipeline = DataPipeline(
        jackhmmer_binary_path=FLAGS.jackhmmer_binary_path,
        uniref90_database_path=FLAGS.uniref90_database_path,
        # below switch back to hmm search 
        template_searcher = hmmsearch.Hmmsearch(
            binary_path= FLAGS.hmmsearch_binary_path,
            hmmbuild_binary_path=FLAGS.hmmbuild_binary_path,
            database_path=FLAGS.pdb_seqres_database_path),
            template_featurizer = templates.HmmsearchHitFeaturizer(
            mmcif_dir=FLAGS.mmcif_dir,
            max_template_date=FLAGS.max_template_date,
            max_hits=MAX_TEMPLATE_HITS,
            kalign_binary_path=FLAGS.kalign_binary_path,
            obsolete_pdbs_path=FLAGS.obsolete_pdbs_path,
            release_dates_path=None),
        use_precomputed_msas=FLAGS.use_precomputed_msas)


    t_0 = time.time()
    if is_prokaryote is None:
        feature_dict = monomer_data_pipeline.process(
            input_fasta_path=fasta_path,
            msa_output_dir=msa_output_dir)
    else:
        feature_dict = monomer_data_pipeline.process(
            input_fasta_path=fasta_path,
            msa_output_dir=msa_output_dir,
            is_prokaryote=is_prokaryote)
    timings['features'] = time.time() - t_0
    return feature_dict,msa_output_dir


def main(argv):
    feature_dict, msa_output_dir = make_template(FLAGS.fasta_paths,FLAGS.fasta_name,
    FLAGS.output_dir)
    print(msa_output_dir+'/template_feature_dict.pkl')
    with open(msa_output_dir+'/template_feature_dict.pkl', 'wb') as f:
        pickle.dump(feature_dict, f, protocol=4)

if __name__ =='__main__':
    flags.mark_flags_as_required([
        'fasta_paths',
        'output_dir',
        'uniref90_database_path',
        'max_template_date',
        'jackhmmer_binary_path',
        'hmmsearch_binary_path',
        'hmmbuild_binary_path',
        'pdb70_database_path',
        'kalign_binary_path',
        'obsolete_pdbs_path',
        'use_precomputed_msas',
        'pdb_seqres_database_path',
        'mmcif_dir'
    ])
    app.run(main)