#!/bin/bash
#A typical runs takes couple of hours but may be much longer
#SBATCH --time=0-20:00:00

#log files:
#SBATCH -e create_msa_error.%A_%a.txt
#SBATCH -o create_msa_out.%A_%a.txt

#qos sets priority, you can set to high or highest but there is a limit of high priority jobs per user: https://wiki.embl.de/cluster/Slurm#QoS
#SBATCH --qos=normal

#Limit the run to a single node
#SBATCH -N 1

#SBATCH --ntasks=8
#SBATCH --mem=80000
module purge
module load AlphaFold/2.0.0-fosscuda-2020b
# module load AlphaFold/2.1.1-fosscuda-2020b-interruptible
input_dir=/g/kosinski/geoffrey/af2_48S_vs_L

python create_pseudo_msa_features.py --fasta_paths=$input_dir/concatenated.fasta --output_dir=$input_dir/msas/concatenated/msas --sequences_path=$input_dir/pdb_seqres.txt