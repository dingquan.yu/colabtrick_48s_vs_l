#!/bin/bash
#SBATCH --time=5-0:00:00
#SBATCH -e predict_structure_error.txt
#SBATCH -o predict_structure_out.txt

#SBATCH --qos=normal

#Limit the run to a single node
#SBATCH -N 1
#SBATCH -p gpu
#SBATCH --gres=gpu:1
#SBATCH -C gpu=A100
#SBATCH --ntasks=1
#SBATCH --mem=512000
feature_dir=/g/kosinski/geoffrey/af2_48S_vs_L/msas/concatenated/msas
output_dir=/scratch/gyu/af2_48S_vs_L
module purge
module load AlphaFold/2.1.1-fosscuda-2020b
XLA_PYTHON_CLIENT_MEM_FRACTION=12.5
time python predict_structure_from_processed_features.py --output_dir=$output_dir --model_preset=monomer --data_dir=/scratch/AlphaFold_DBs/ --fasta_name=huge_complex --feature_dict_path=$feature_dir/feature_dict.pkl --num_cycle=3
