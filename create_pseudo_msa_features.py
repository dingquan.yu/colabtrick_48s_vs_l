import json
import os
import pathlib
import pickle
import random
import shutil
import sys
import time
from typing import Dict, Union, Optional


from absl import app
from absl import flags
from absl import logging

from alphafold.data.pipeline import make_sequence_features
from alphafold.common import residue_constants


import numpy as np
import Bio
from Bio import SeqIO
import os
from typing import Any, Mapping, MutableMapping, Optional, Sequence, Union
from absl import logging
from alphafold.data import parsers

import numpy as np
from alphafold.model import data

flags.DEFINE_string(
    'fasta_paths', None, 'Paths to FASTA files, each containing a prediction '
    'target that will be folded one after another. If a FASTA file contains '
    'multiple sequences, then it will be folded as a multimer. Paths should be '
    'separated by commas. All FASTA paths must have a unique basename as the '
    'basename is used to name the output directories for each prediction.')

flags.DEFINE_string(
    'sequences_path', None, 'Paths to the file that contains all sequences, each containing a prediction '
    'target that will be folded one after another. If a FASTA file contains '
    'multiple sequences, then it will be folded as a multimer. Paths should be '
    'separated by commas. All FASTA paths must have a unique basename as the '
    'basename is used to name the output directories for each prediction.')

flags.DEFINE_string('output_dir', None, 'Path to a directory that will '
                    'store the results.')

FLAGS = flags.FLAGS


def make_features(
    fasta_path:str,
    sequences_path,
    output_path:str):
    """create pseudo msa features"""

    with open(fasta_path) as f:
      input_fasta_str = f.read()
    input_seqs, input_descs = parsers.parse_fasta(input_fasta_str)
    input_sequence = input_seqs[0]
    input_description = input_descs[0]
    num_res = len(input_sequence)

    sequence_feature = make_sequence_features(input_sequence,input_description,num_res)
    
    # update residue index
    records = list(SeqIO.parse(sequences_path,format='fasta'))
    new_residue_index = np.array([])
    for i in range(len(records)):
        if i ==0:
            curr_seq_len = len(records[i].seq)
            curr_region = np.arange(0,curr_seq_len)
            new_residue_index = np.concatenate((new_residue_index,curr_region),axis=0)

        else:
            curr_start = new_residue_index[-1] +1 # current starting point 
            curr_seq_len = len(records[i].seq)
            curr_region = np.arange(curr_start + 200,curr_start + 200 + curr_seq_len)
            new_residue_index = np.concatenate((new_residue_index,curr_region),axis=0)

    # prepare msa features
    msas = []
    deletion_matrices = []
    msas.append([input_sequence])
    deletion_matrices.append([[0]*len(input_sequence)])
    pseudo_del_mtx = np.zeros((1,num_res),dtype=np.int32)
    pseudo_msa = [residue_constants.HHBLITS_AA_TO_ID[res] for res in input_sequence]
    pseudo_msa = np.array([pseudo_msa],dtype=np.int32)
    pseudo_num_alignments = np.array([1]*num_res)
    msa_features = {
        'deletion_matrix_int' : pseudo_del_mtx,
        'msa':pseudo_msa,
        'num_alignments':pseudo_num_alignments,
        'residue_index' : new_residue_index.astype(np.int32)
    }

    final_dict = {**sequence_feature,**msa_features}
    pickle.dump(final_dict,open(f"{output_path}/msa_feature_dict.pkl",'wb'))


def main(argv):
    input_fasta = FLAGS.fasta_paths
    output_path = FLAGS.output_dir
    sequences_path = FLAGS.sequences_path
    make_features(input_fasta,sequences_path,output_path)

if __name__ =='__main__':
    app.run(main)
