#!/bin/bash
#SBATCH --time=0-2:00:00
#SBATCH -e search_templates_error.%A_%a.txt
#SBATCH -o search_templates_out.%A_%a.txt

#SBATCH --qos=normal

#Limit the run to a single node
#SBATCH -N 1
#SBATCH -p htc
#SBATCH --ntasks=2
#SBATCH --mem=64000
# first prepare template features

export ALPHAFOLD_JACKHMMER_N_CPU=$SLURM_NTASKS
export ALPHAFOLD_HHBLITS_N_CPU=$SLURM_NTASKS
module load AlphaFold/2.1.1-fosscuda-2020b-interruptible
input_dir=/g/kosinski/geoffrey/af2_48S_vs_L
python run_template_features.py --fasta_paths=$input_dir/concatenated.fasta --fasta_name=concatenated --output_dir=$input_dir/msas/ --pdb_seqres_database_path=$input_dir/pdb_seqres.txt --use_precomputed_msas=True --mmcif_dir=$input_dir/template_mmcif --obsolete_pdbs_path=/scratch/AlphaFold_DBs/pdb_mmcif/obsolete.dat --jackhmmer_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/HMMER/3.3.2-gompic-2020b/bin/jackhmmer --kalign_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/Kalign/3.3.1-GCCcore-10.2.0/bin/kalign --pdb70_database_path=/scratch/AlphaFold_DBs/pdb70/pdb70 --hmmsearch_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/HMMER/3.3.2-gompic-2020b/bin/hmmsearch --hmmbuild_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/HMMER/3.3.2-gompic-2020b/bin/hmmbuild --max_template_date=2050-01-01 --uniref90_database_path=/scratch/AlphaFold_DBs/uniref90/uniref90.fasta  
# python run_template_features.py --fasta_paths=$PWD/$curr_file --fasta_name=$fasta_name --output_dir=$PWD --mmcif_dir=$PWD/mmcif/ --obsolete_pdbs_path=/scratch/AlphaFold_DBs/pdb_mmcif/obsolete.dat --jackhmmer_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/HMMER/3.3.2-gompic-2020b/bin/jackhmmer --kalign_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/Kalign/3.3.1-GCCcore-10.2.0/bin/kalign --pdb70_database_path=/scratch/AlphaFold_DBs/pdb70/pdb70 --hmmsearch_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/HMMER/3.3.2-gompic-2020b/bin/hmmsearch --hmmbuild_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/HMMER/3.3.2-gompic-2020b/bin/hmmbuild --max_template_date=2050-01-01 --uniref90_database_path=/scratch/AlphaFold_DBs/uniref90/uniref90.fasta  --pdb_seqres_database_path=$PWD/pdb_seqres/pdb_seqres.txt --use_precomputed_msas=True

# for f in $files
# do
#     fasta_name=`echo $f | cut -d'.' -f 1`
#     echo "now processing " $fasta_name
#     python run_template_features.py --fasta_paths=$PWD/$f --fasta_name=$fasta_name --output_dir=$PWD --mmcif_dir=/scratch/AlphaFold_DBs/pdb_mmcif/mmcif_files/ --obsolete_pdbs_path=/scratch/AlphaFold_DBs/pdb_mmcif/obsolete.dat --jackhmmer_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/HMMER/3.3.2-gompic-2020b/bin/jackhmmer --kalign_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/Kalign/3.3.1-GCCcore-10.2.0/bin/kalign --pdb70_database_path=/scratch/AlphaFold_DBs/pdb70/pdb70 --hmmsearch_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/HMMER/3.3.2-gompic-2020b/bin/hmmsearch --hmmbuild_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/HMMER/3.3.2-gompic-2020b/bin/hmmbuild --max_template_date=2050-01-01 --uniref90_database_path=/scratch/AlphaFold_DBs/uniref90/uniref90.fasta  --pdb_seqres_database_path=/scratch/AlphaFold_DBs/pdb_seqres/pdb_seqres.txt --use_precomputed_msas=True
# done

# echo "now will create final features"
# # then generate final features
# cat *.fasta >> concatenated.fasta
# python final_features.py --jackhmmer_binary_path=/g/easybuild/x86_64/CentOS/7/haswell/software/HMMER/3.3.2-gompic-2020b/bin/jackhmmer --uniprot_database_path=/scratch/AlphaFold_DBs/uniprot/uniprot.fasta --input_fasta_path=$PWD/concatenated.fasta --feature_path=$PWD
