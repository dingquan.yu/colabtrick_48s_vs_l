import pickle 
from absl import app
from absl import flags
import os
flags.DEFINE_string('input_dir', None, 'Path to directory of supporting data.')


FLAGS = flags.FLAGS
def main(argv):

    input_dir = FLAGS.input_dir
    msa_path = os.path.join(input_dir,'msa_feature_dict.pkl')
    template_path = os.path.join(input_dir,'template_feature_dict.pkl')
    msa_feature = pickle.load(open(msa_path,'rb'))
    template_feature = pickle.load(open(template_path,'rb'))
    final_dict = {**msa_feature,**template_feature}
    pickle.dump(final_dict,open(os.path.join(input_dir,'feature_dict.pkl'),'wb'))

if __name__ =='__main__':
    app.run(main)
